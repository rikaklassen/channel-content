- Costa, Imasha and Vivienne Clarke. "[Mask wearing in hospitals should remain as covid 'hasn't gone away'](https://www.irishexaminer.com/news/arid-41115766.html)." *Irish Examiner*, April 13, 2023. Retrieved April 15, 2023. [Archived](https://web.archive.org/web/20230414072224/https://www.irishexaminer.com/news/arid-41115766.html) from the original on April 14 via Wayback Machine.
- Pierre-Louis, Kendra. "[Three Years Later, Covid-19 Is Still a Health Threat. Journalism Needs to Reflect That](https://niemanreports.org/articles/three-years-later-covid-19-is-still-a-health-threat-journalism-needs-to-reflect-that/)." *Nieman*, April 12, 2023. Retrieved April 12, 2023. [Archived](https://web.archive.org/web/20230414022016/https://niemanreports.org/articles/three-years-later-covid-19-is-still-a-health-threat-journalism-needs-to-reflect-that/) from the original on April 14, 2023.

## Support
If you find this document useful and wish to see continous updates, please consider tipping via [PayPal](https://paypal.me/bglamours).
## License and Copyright
[![Creative Commons Public Domain CC0](https://licensebuttons.net/p/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)
This repository and all files, documents, and source code contained in this repository, to the exant possible under law, is available under the [Creative Commons Zero 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/) license. Effectively, this project is public domain. Note that some illustrations and photographs are not public domain, and those can be found in [`COPYRIGHT.md`](./COPYRIGHT.md).

Last updated April 15, 2023 (16:20:00 UTC).
